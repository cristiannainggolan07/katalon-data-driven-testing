<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite CO</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>211f7063-2c57-479f-83f9-5adea2492124</testSuiteGuid>
   <testCaseLink>
      <guid>5334cbce-ca21-4632-a7d7-dacf580ec964</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC02_CO/standard-CO - VarLoc</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f316dd3c-99df-4d4e-9b00-0f2fbe1eea52</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data CO</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>f316dd3c-99df-4d4e-9b00-0f2fbe1eea52</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>FirstName</value>
         <variableId>288dc528-1dad-4ade-a7cf-cd15c4ddd618</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f316dd3c-99df-4d4e-9b00-0f2fbe1eea52</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>LastName</value>
         <variableId>b6961e84-682d-480d-9b1b-f395ae2067f8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f316dd3c-99df-4d4e-9b00-0f2fbe1eea52</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Zip</value>
         <variableId>bab3f1d2-e689-42c1-93da-93243db5e743</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
