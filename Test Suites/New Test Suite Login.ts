<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>7d057061-b614-4969-bedc-bd6f50463fc3</testSuiteGuid>
   <testCaseLink>
      <guid>a81baa7d-5bc3-444d-a03c-42544e656a94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC01_Login/Login - LocVar</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fb59f144-5457-43f5-ba38-0f8448a7045c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>fb59f144-5457-43f5-ba38-0f8448a7045c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>df1bc48b-dc45-4084-be3d-d12c23df1c2b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>fb59f144-5457-43f5-ba38-0f8448a7045c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>dd6bb73b-6dae-44a4-9aa9-8a8fc9e9820a</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
